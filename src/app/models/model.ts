export abstract class Model {
  constructor(private model: Array<any>) {}

  getList(): Array<any> {
    return this.model || [];
  }

  getById(id: number): object {
    return this.model.find(item => Number(item.id) === Number(id)) || {};
  }

  getIndex(id: number): number {
    return this.model.findIndex(item => item.id === id);
  }

  add(data: object): Array<any> {
    this.model.push(data);
    return this.model;
  }

  edit(id: number, data: object): Array<any> {
    const index = this.getIndex(id);
    if (index > -1) {
      this.model.splice(index, 1, data);
    }
    return this.model;
  }

  delete(id): Array<any> {
    const index = this.getIndex(id);
    if (index > -1) {
      this.model.splice(index, 1);
    }
    return this.model;
  }
}
