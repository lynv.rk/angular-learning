import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {
  @Input('appHighlight') bgColor: string;

  constructor(private el: ElementRef) { }

  // @HostListener('click')

  @HostListener('mouseenter') onMouseEnter() {
    this.highlight(this.bgColor || '#f1c5c5');
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.highlight(null);
  }

  private highlight(color: string) {
    this.el.nativeElement.style.backgroundColor = color;
  }

}
