import { Component, Inject } from '@angular/core';
import { Title } from "@angular/platform-browser";

import { APP_CONFIG, AppConfig } from 'src/config/app.config';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title: string;

  constructor(
    @Inject(APP_CONFIG) config: AppConfig,
    private titleService: Title,
  ) {
    this.title = config.title;
    this.titleService.setTitle(config.title);
  }
}
