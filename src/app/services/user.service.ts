import { Injectable, InjectionToken, Inject } from '@angular/core';
import { Model } from '../models/model';
import { USERS } from 'src/mock/users';

// Inject using value
export const injectionToken = new InjectionToken<Array<any>>('USERS', {
  providedIn: 'any',
  factory: () => USERS
});

@Injectable({
  providedIn: 'root'
})
export class UserService extends Model{

  constructor(@Inject(injectionToken) model) {
    super(model);
  }
}
