import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  id: number;
  user: any;
  userForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.getUser();
    this.initForm();
  }

  async getUser() {
    this.id = Number(this.route.snapshot.params.id);
    this.user = this.userService.getById(this.id);
    if (Number(this.id) && !this.user || !Object.keys(this.user).length) {
      this.router.navigate(['/not-found']);
    }
  }

  initForm() {
    this.userForm = this.formBuilder.group({
      id: [this.user.id],
      name: [this.user.name],
      email: [this.user.email],
      address: [this.user.address],
    });
  }

  async submit() {
    await this.userService.edit(this.id, this.userForm.value);
    this.router.navigate(['/user']);
  }

  cancel(): void {
    this.router.navigate(['/user']);
  }
}
