import { Component, OnInit, Self, SkipSelf } from '@angular/core';
import { BROWSER_STORAGE, StorageService as BrowserStorageService } from 'src/app/services/storage.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  providers: [
    BrowserStorageService,
    { provide: BROWSER_STORAGE, useFactory: () => sessionStorage }
  ]
})
export class UserComponent implements OnInit {
  users: Array<any>;

  constructor(
    @Self() private sessionStorageService: BrowserStorageService,
    @SkipSelf() private localStorageService: BrowserStorageService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.setLocalStorage();
    this.setSessionStorage();
    this.users = this.userService.getList();
  }

  setSessionStorage() {
    this.sessionStorageService.set('test', 'Angular Learning - sessionStorageService');
  }

  setLocalStorage() {
    this.localStorageService.set('test', 'Angular Learning - localStorageService');
  }

  delete(id) {
    this.users = this.userService.delete(id);
  }

}
