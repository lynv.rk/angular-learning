import { InjectionToken } from '@angular/core';
import { environment } from '../environments/environment';
import { TITLE } from './config';

export class AppConfig {
  apiEndpoint: string;
  title: string;
}

export const APP_DI_CONFIG: AppConfig = {
  apiEndpoint: environment.apiEndpoint,
  title: TITLE
};

// Non-class dependencies
export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
