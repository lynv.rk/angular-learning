# Sevices, dependency injection
- Service: fetching data từ server, validating user input, logging ...
- Service có thể được phụ thuộc vào các service khác
- DI được nối vào Angular framework và sử dụng bất cứ đâu nhằm cung cấp những component mới với service hoặc bất cứ thứ gì mà nó cần
- Để định nghĩa một class như một service trong Angular, sử dụng @Injectable() decorator để cung cấp metadata
- Có thể đăng ký các provider trong trên metadata của service (in the @Injectable() decorator), hoặc trong @NgModule() @Directive() hoặc @Component() metadata
=> Đăng ký provider trong @Injectable() metadata cũng cho phép Angular tối ưu hóa ứng dụng (remove service từ compiled app nếu không sử dụng)

## DI providers:
### Alternative class providers
- EX: { provide: Logger, useClass: EvenBetterLogger } | providers: [EvenBetterLogger]
- Not aliased! Creates two instances of 'NewLogger'{ provide: OldLogger, useClass: NewLogger}
- Alias OldLogger w/ reference to NewLogger { provide: OldLogger, useExisting: NewLogger}

### Value providers: (Use when you want to inject a string, function, or object.)
- Non-class dependencies { provide: APP_CONFIG, useValue: APP_DI_CONFIG } (Demo file: /src/config/app.config.ts. Using: /src/app/app.component.ts)

### Factory providers: (Use when you need to create a dependent value dynamically, based on information you won't have until run time (information that changes repeatedly))
- EX 1: { provide: nameService, useFactory: nameServiceFactory, deps: [Logger, UserService] }
  + file: demo.service.ts

  class DemoService {
    constructor(
      private logger: Logger,
      private isAuthorized: boolean) { }

    getAuth() {
      let auth = this.isAuthorized ? 'authorized ' : 'unauthorized';
      this.logger.log(authorized);
    }
  }
  + file: demo.service.provider.ts (demoServiceProvider)

  let demoServiceFactory = (logger: Logger, userService: UserService) => {
    return new DemoService(logger, userService.isAuthorized);
  };
  export let demoServiceProvider = {
    provide: DemoService,
    useFactory: demoServiceFactory,
    deps: [Logger, UserService]
  };
  + Using
  In component decorator: providers: [ demoServiceProvider ]

- EX 2:
  let NB_INSTANCES = 0;
  export const UNIQUE_ID = new InjectionToken<string>("UNIQUE_ID");
  export const UNIQUE_ID_PROVIDER = {
      provide: UNIQUE_ID,
      useFactory: () => "my-unique-id-" + (NB_INSTANCES++)
  };
  + Using (providers: [UNIQUE_ID_PROVIDER])
    constructor(@Inject(UNIQUE_ID) public id: string)

### Predefined tokens and multiple providers
- Angular provides a number of built-in injection-token constants that you can use to customize the behavior of various systems.
- { provide: PLATFORM_INITIALIZER, useFactory: platformInitialized, multi: true    },
  More const: https://angular.io/api?type=const

### Tree-shakable providers
- @Injectable({
    providedIn: 'root',
  }) => when compile removes code from the final bundle if the app doesn't reference that code
- @Injectable() => configure the injector of a specific NgModule or component

## Using @Host(), @Optional(), @Self(), @SkipSelf() (DI)
- @Host(), @Optional()
  + khi 1 component gọi template như sau
  + <app-hero-bio> <app-hero-contact></app-hero-contact> </app-hero-bio>
  + thì component `app-hero-contact` sẽ có cha là `app-hero-bio`
  + Khi đó component `app-hero-bio` đã gọi DI của service HeroCacheService rồi (Using)
  + giờ component con `app-hero-contact` muốn dùng nữa thì không cần DI lại trong metadata
  + dùng  @Host() để get a reference service từ component cha (app-hero-bio) (Angular throws an error if the parent lacks that service, even if a component higher in the component tree includes it.
  + @Optional() // ok if the logger doesn't exist)
  + View more: https://angular.io/guide/dependency-injection-in-action#make-a-dependency-optional-and-limit-search-with-host

- @Self, @SkipSelf()
  + Using the @Self decorator, the injector only looks at the component's injector for its providers
  + The @SkipSelf decorator allows you to skip the local injector and look up in the hierarchy to find a provider that satisfies this dependency
  + EX: ví dụ về sử dụng storage như trong bài học (file /src/services/storage.service.ts). Được sử dụng tại component /component/user.component.ts

# Pipe
- get data, transform them, or format, show data to users
- EX: uppercase string, format date ...
- angular pipes https://angular.io/api?type=pipe
- custom pipe:
  + import { Pipe, PipeTransform } from '@angular/core';
    @Pipe({name: 'exponentialStrength'})
    export class ExponentialStrengthPipe implements PipeTransform {
      transform(value: number, exponent?: number): number {
        return Math.pow(value, isNaN(exponent) ? 1 : exponent);
      }
    }
  + Declarations: Must include pipe in the declarations array of the AppModule or inject pipe into a class, must provide it in the providers array of your NgModule.
  + Use custom pipe the same way use built-in pipes.

# Attribute directives
- An Attribute directive changes the appearance or behavior of a DOM element.
- EX: /src/app/directives/highlight.directive.ts (Use: user.component.html)
